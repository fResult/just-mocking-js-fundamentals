/**
 * With our current usage of the mock function,
 * we have to manually keep track of the original
 * implementation so we can clean up after
 * ourselves to keep our tests idempotent. Let’s
 * see how jest.spyOn can help us avoid the
 * bookkeeping and simplify our situation.
 *
 * Task: use `spyOn` to replace `fn`
 *
 * Execute: Use `npx jest --watch src/__tests__/spy.js` to watch the test
 */

const thumbWar = require("../thumb-war")
const utils = require("../utils")

const player1 = "Korn Zilla"
const player2 = "fResult"

test("returns winner", () => {
  jest.spyOn(utils, "getWinner")
  utils.getWinner.mockImplementation((p1, _p2) => p1)

  const winner = thumbWar(player1, player2)
  expect(winner).toBe(player1)
  expect(utils.getWinner.mock.calls).toEqual([
    [player1, player2],
    [player1, player2],
  ])

  // cleanup
  utils.getWinner.mockRestore()
})

/**
 * Hints:
 * - https://jestjs.io/docs/en/jest-object#jestspyonobject-methodname
 *
 * Checkout master branch to see the answer.
 */
