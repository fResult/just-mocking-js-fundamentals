/**
 * So far we’re still basically monkey-patching the utils
 * module which is fine, but could lead to problems in the
 * future, especially if we want to mock a ESModule export
 * which doesn’t allow this kind of monkey-patching on
 * exports. Instead, let’s mock the entire module so when
 * our test subject requires the file they get our mocked
 * version instead.
 *
 * Task: refactor the code to mock the entire module.
 *
 * Execute: Use `npx jest --watch src/__tests__/inline-module-mock.js` to watch the test
 */

const thumbWar = require('../thumb-war')
const utils = require('../utils')

const player1 = 'Korn Zilla'
const player2 = 'fResult'

jest.mock('../utils', () => {
  return {
    getWinner: jest.fn((p1, _p2) => p1),
  }
})

test('returns winner', () => {
  const winner = thumbWar(player1, player2)

  expect(winner).toBe(player1)
  expect(utils.getWinner.mock.calls).toEqual([
    [player1, player2],
    [player1, player2],
  ])

  // cleanup
  utils.getWinner.mockReset()
})

/**
 * Hints:
 * - https://jestjs.io/docs/en/es6-class-mocks#calling-jestmockdocsenjest-objectjestmockmodulename-factory-options-with-the-module-factory-parameter
 *
 * Checkout master branch to see the answer.
 */
