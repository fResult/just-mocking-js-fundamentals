/**
 * Task: implement a `spyOn`.
 *
 * Execute: Use `npx jest --watch src/no-framework/spy.js` to watch the test
 */

const assert = require("assert")
const thumbWar = require("../thumb-war")
const utils = require("../utils")

/**
 * @template T
 *
 * @param { T } obj
 * @param { keyof T } prop
 */
function spyOn(obj, prop) {
  const origFunction = obj[prop]
  obj[prop] = fn()

  obj[prop].mockRestore = () => (obj[prop] = origFunction)
}

function fn(impl = () => {}) {
  function mockFn(...args) {
    mockFn.mock.calls.push(args)

    return impl(...args)
  }

  mockFn.mockImplementation = (newImpl) => (impl = newImpl)
  mockFn.mock = { calls: [] }

  return mockFn
}

const player1 = "Korn Zilla"
const player2 = "fResult"

spyOn(utils, "getWinner")

utils.getWinner.mockImplementation((p1, _p2) => p1)

const winner = thumbWar(player1, player2)

assert.strictEqual(winner, player1)
assert.deepStrictEqual(utils.getWinner.mock.calls, [
  [player1, player2],
  [player1, player2],
])

// cleanup
utils.getWinner.mockRestore()

/**
 * Checkout master branch to see the answer.
 */
