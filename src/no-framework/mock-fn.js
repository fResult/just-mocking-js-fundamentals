/**
 * After using the assertions provided by Jest, let's implement them ourselves.
 *
 * Task: Write a function `fn` that creates a mock function has `mock.calls`.
 *
 * Execute: Use `npx jest --watch src/no-framework/mock-fn.js` to watch the test
 */

const assert = require('assert')
const thumbWar = require('../thumb-war')
const utils = require('../utils')

/**
 * @template T
 * @extends { any }
 * @typedef { (...args: T[]) => T } AnyFunction
 */
/**
 * @typedef { AnyFunction & { mock: { calls: Parameters<AnyFunction>[] } } } MockFunction
 */

/**
 * @param { AnyFunction } impl
 * @return { MockFunction }
 */
function fn(impl) {
  /**
   * @type { MockFunction }
   */
  const mockFn = (...args) => {
    mockFn.mock.calls.push(args)
    return impl(...args)
  }

  mockFn.mock = { calls: [] }

  return mockFn
}

const player1 = 'Korn Zilla'
const player2 = 'fResult'

const origGetWinner = utils.getWinner
utils.getWinner = fn((p1, _p2) => p1)
const winner = thumbWar(player1, player2)

console.log(utils.getWinner.mock.calls)

assert.strictEqual(winner, player1)
assert.deepStrictEqual(utils.getWinner.mock.calls, [
  [player1, player2],
  [player1, player2],
])

// cleanup
utils.getWinner = origGetWinner

/**
 * Checkout master branch to see the answer.
 */
