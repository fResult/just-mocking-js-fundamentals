/**
 * Task: refactor the code to mock the entire module.
 *
 * Execute: Use `npx jest --watch src/no-framework/inline-module-mock.js` to watch the test
 */

function fn(impl = () => {}) {
  const mockFn = (...args) => {
    mockFn.mock.calls.push(args)
    return impl(...args)
  }

  mockFn.mock = { calls: [] }
  // mockFn.mockReset = () => delete require.cache

  return mockFn
}

function mock(path, createMockModule) {
  const resolvedPath = require.resolve(path)

  require.cache[resolvedPath] = {
    id: resolvedPath,
    filename: resolvedPath,
    loaded: true,
    exports: createMockModule(),
  }
  console.log('cache', require.cache[resolvedPath])
}

mock('../utils', function createMockUtils() {
  return {
    getWinner: fn((p1, _p2) => p1),
  }
})

const assert = require('assert')
const thumbWar = require('../thumb-war')
const utils = require('../utils')

const player1 = 'Korn Zilla'
const player2 = 'fResult'

const winner = thumbWar(player1, player2)
assert.strictEqual(winner, player1)
assert.deepStrictEqual(utils.getWinner.mock.calls, [
  [player1, player2],
  [player1, player2],
])

// cleanup
delete require.cache[require.resolve('../utils')]

/**
 * Hints:
 * - https://nodejs.org/api/modules.html#modules_caching
 *
 * Checkout master branch to see the answer.
 */
