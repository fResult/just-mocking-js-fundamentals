function fn(impl = () => {}) {
  function mockFn(...args) {
    mockFn.mock.calls.push(args)

    return impl(...args)
  }

  mockFn.mock = { calls: [] }

  return mockFn
}

module.exports = { getWinner: fn((p1, _p2) => p1) }
