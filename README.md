<h1 align="center">
  <a href="https://testingjavascript.com/courses/javascript-mocking-fundamentals">JavaScript Mocking Fundamentals</a>
</h1>

<div align="center">
  <h2><a href="https://testingjavascript.com">TestingJavaScript.com</a></h2>
  <a href="https://testingjavascript.com">
    <img
      width="500"
      alt="Learn the smart, efficient way to test any JavaScript application."
      src="https://kentcdodds.com/images/testingjavascript-promo/tjs-4.jpg"
    />
  </a>
</div>

<hr />

<p align="center" style="font-size: 1.2rem;">
  Learn how mocking in JavaScript tests works by implementing them from scratch!
</p>

<hr />

In this material, we have a set of `no-framework` tests that correspond to a set
of jest tests (in the `__tests__` directory). The idea is that (with the
exception of the first test), you look at the jest version first, then see how
that would be implemented without a testing framework.

Order of material:

1.  `monkey-patching.js` (no jest version)
2.  `mock-fn.js`
3.  `spy.js`
4.  `inline-module-mock.js`
5.  `external-mock-module.js`

The files are intended to test the `thumb-war.js` module and mock the `utils`
module.

To run the tests, run `npx jest`. To start watch mode run `npx jest --watch`

## Custom jest runner.

You can definitely run the `no-framework` files just using `node` (like this:
`node src/no-framework/monkey-patching.js`), but in an effort to make running
these easier, I created a custom jest runner that uses jest to run the files,
but allow them to be run without the jest testing framework. It's really cool.
It uses [`create-jest-runner`](https://www.npmjs.com/package/create-jest-runner)
and should probably be published eventually.

<h2 align="center">
  All Testing JS
</h2>

1. [Fundamentals of Testing in JavaScript](https://gitlab.com/fResult/just-testing-javascript)
2. [JavaScript Mocking Fundamentals](https://gitlab.com/fResult/just-mocking-js-fundamentals) << You are here
3. [Static Analysis Testing JavaScript Applications](https://gitlab.com/fResult/just-js-static-testing-tools)
4. [Use DOM Testing Library to test any JS framework](#)
5. [Configure Jest for Testing JavaScript Applications](#)
6. [Test React Components with Jest and React Testing Library](#)
7. [Install, Configure, and Script Cypress for JavaScript Web Applications](#)
8. [Test Node.js Backends](#)
